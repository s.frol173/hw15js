buttons = document.getElementsByTagName("button");
console.log(buttons);
const enter = buttons[0];
const s = buttons[1];
const e = buttons[2];
const o = buttons[3];
const n = buttons[4];
const l = buttons[5];
const z = buttons[6];
const tab = buttons[7];

document.addEventListener("keydown", (event) => {
  enter.classList.remove("blue");
  s.classList.remove("blue");
  e.classList.remove("blue");
  o.classList.remove("blue");
  n.classList.remove("blue");
  l.classList.remove("blue");
  z.classList.remove("blue");
  tab.classList.remove("blue");

  switch (event.code) {
    case "Enter":
      enter.classList.add("blue");
      break;
    case "KeyS":
      s.classList.add("blue");
      break;
    case "KeyE":
      e.classList.add("blue");
      break;
    case "KeyO":
      o.classList.add("blue");
      break;
    case "KeyN":
      n.classList.add("blue");
      break;
    case "KeyL":
      l.classList.add("blue");
      break;
    case "KeyZ":
      z.classList.add("blue");
      break;
    case "Tab":
      tab.classList.add("blue");
      break;
  }

  console.log(event);
});
